angular
	.module('AccountManagement', [])
	.controller("AccountManagement", function($scope, $http,$location) {

		$scope.form = false;
		$scope.accounts = [];
		$scope.accountForm = {
			id: 1,
			name: "",
			email: "",
			phone: ""
		};

		_refreshAccountData();


		$scope.submitAccount = function() {

			var method = "";
			var url = "";

			if ($scope.accountForm.id == -1) {
				method = "POST";
				url = '/accounts';
			} else {
				method = "PUT";
				url = '/accounts/' + $scope.accountForm.id;
			}

			$http({
				method: method,
				url: url,
				data: angular.toJson($scope.accountForm),
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(_success, _error);
		};

		$scope.createAccount = function() {
			_clearFormData();
			$scope.form = true;
		}

		// HTTP DELETE- delete employee by Id
		// Call: http://localhost:8080/employee/{empId}
		$scope.removeAccount = function(account) {
			$http({
				method: 'DELETE',
				url: '/accounts/' + account.id
			}).then(_success, _error);
		};

		// In case of edit
		$scope.editAccount = function(account) {
			$scope.form = true;
			$scope.accountForm.id = account.id;
			$scope.accountForm.name = account.name;
			$scope.accountForm.phone = account.phone;
			$scope.accountForm.email = account.email;

		};

		// Private Method  
		// HTTP GET- get all employees collection
		// Call: http://localhost:8080/employees
		function _refreshAccountData() {
			$http({
				method: 'GET',
				url: '/accounts'
			}).then(
				function(res) { // success
					$scope.accounts = res.data;
				},
				function(res) { // error
					console.log("Error: " + res.status + " : " + res.data);
				}
			);
		}

		function _success(res) {
			_refreshAccountData();
			_clearFormData();
			$scope.form = false;
		}

		function _error(res) {
			var data = res.data;
			var status = res.status;
			var header = res.header;
			var config = res.config;
			alert("Error: " + status + ":" + data);
		}

		// Clear the form
		function _clearFormData() {
			$scope.accountForm.id = -1;
			$scope.accountForm.name = "";
			$scope.accountForm.phone = "";
			$scope.accountForm.email = ""
		};
	});