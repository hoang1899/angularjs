
angular
	.module("index", ['ngRoute', 'AccountManagement'])

	.config(function($routeProvider, $locationProvider) {
		$routeProvider
			.when('/account-mgmt', {
				templateUrl: 'views/account/list.html',
				controller: 'AccountManagement'
			})
			.when('/login', {
				templateUrl: 'views/login.html'
			})
			.otherwise({
				redirectTo: '/login'
			});
		$locationProvider.html5Mode(true);

	});

