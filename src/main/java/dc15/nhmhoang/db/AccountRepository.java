package dc15.nhmhoang.db;

import org.springframework.data.repository.CrudRepository;

import dc15.nhmhoang.models.Account;

public interface AccountRepository extends CrudRepository<Account, Integer> {

}
