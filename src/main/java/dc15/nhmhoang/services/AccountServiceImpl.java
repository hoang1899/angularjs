package dc15.nhmhoang.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dc15.nhmhoang.db.AccountRepository;
import dc15.nhmhoang.models.Account;

@Service
public class AccountServiceImpl implements AccountService {

	
	private AccountRepository accountRepository;
	
	
	@Autowired
	public void setAccountRepository(AccountRepository accountRepository) {
		this.accountRepository = accountRepository;
	}
	
	
	@Override
	public List<Account> listAll() {
		return (List<Account>) accountRepository.findAll();
	}

	@Override
	public void remove(Integer id) {
		accountRepository.deleteById(id);;
	}



	@Override
	public Account add(Account account) {
		return accountRepository.save(account);
	}

	@Override
	public Account edit(Integer id, Account account) {
		account.setId(id);
		return accountRepository.save(account);

	}

	

	@Override
	public Account getById(Integer id) {
		Optional<Account> foundAccount = accountRepository.findById(id);
		return foundAccount.isPresent()? foundAccount.get() : null;
		
	}

}
