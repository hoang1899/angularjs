angular
	.module('AccountManagement', [])
	.controller("AccountManagement", function($scope, $http) {


		$scope.showForm = false;
		$scope.formClicked = "";
		$scope.toggleForm = function(account, formClicked) {
			$scope.formClicked = formClicked;
			$scope.showForm = !$scope.showForm;

			if (formClicked == 'create') createAccount();
			else editAccount(account);
		}

		$scope.accounts = [];
		$scope.accountForm = {
			id: 1,
			name: "",
			gender: "",
			role: "",
			email: "",
			phone: ""
		};

		_refreshAccountData();


		$scope.submitAccount = function() {

			var method = "";
			var url = "";

			if ($scope.accountForm.id == -1) {
				method = "POST";
				url = '/accounts';
			} else {
				method = "PUT";
				url = '/accounts/' + $scope.accountForm.id;
			}

			$http({
				method: method,
				url: url,
				data: angular.toJson($scope.accountForm),
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(_success, _error);
		};

		function createAccount() {
			_clearFormData();
			$scope.form = true;
		}


		$scope.removeAccount = function(account) {
			$http({
				method: 'DELETE',
				url: '/accounts/' + account.id
			}).then(_success, _error);
		};

		function editAccount(account) {
			$scope.accountForm.id = account.id;
			$scope.accountForm.name = account.name;
			$scope.accountForm.phone = account.phone;
			$scope.accountForm.email = account.email;

		};


		function _refreshAccountData() {
			$http({
				method: 'GET',
				url: '/accounts'
			}).then(
				function(res) { // success
					$scope.accounts = res.data;
				},
				function(res) { // error
					console.log("Error: " + res.status + " : " + res.data);
				}
			);
		}

		function _success(res) {
			_refreshAccountData();
			_clearFormData();
		}

		function _error(res) {
			var data = res.data;
			var status = res.status;
			var header = res.header;
			var config = res.config;
			alert("Error: " + status + ":" + data);
		}

		function _clearFormData() {
			$scope.accountForm.id = -1;
			$scope.accountForm.name = "";
			$scope.accountForm.phone = "";
			$scope.accountForm.email = ""
		};

		$scope.currentPage = 1;
		$scope.itemsPerPage = 3;
		
		function setPage(pageNo){
			$scope.currentPage = pageNo;
//			$scope.displayingItems = accounts.splice()
		}

	})
	.directive('addUpdateDelete', function() {
		return {
			templateUrl: 'views/account/form.html',
			restrict: 'E',
			transclude: true,
			replace: true,
			scope: true,
			link: function postLink(scope, element, attrs) {
				scope.$watch(attrs.visible, function(value) {
					if (value == true)
						$(element).modal('show');
					else
						$(element).modal('hide');
				});

				$(element).on('shown.bs.modal', function() {
					scope.$apply(function() {
						scope.$parent[attrs.visible] = true;
					});
				});

				$(element).on('hidden.bs.modal', function() {
					scope.$apply(function() {
						scope.$parent[attrs.visible] = false;
					});
				});
			}
		}
	});