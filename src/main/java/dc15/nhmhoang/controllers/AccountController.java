package dc15.nhmhoang.controllers;

import java.util.List;
import static java.util.stream.Collectors.toList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import dc15.nhmhoang.models.Account;
import dc15.nhmhoang.models.requests.AccountRequestDTO;
import dc15.nhmhoang.models.responses.AccountResponseDTO;
import dc15.nhmhoang.services.AccountService;
import dc15.nhmhoang.tools.Mapper;

@RestController
@RequestMapping("/accounts")
public class AccountController {

	private AccountService accountService;
	private Mapper mapper;

	@Autowired
	public void setAccountService(AccountService accountService) {
		this.accountService = accountService;
	}

	@Autowired
	public void setMapper(Mapper mapper) {
		this.mapper = mapper;
	}

	@GetMapping
	public ResponseEntity<List<AccountResponseDTO>> getAccounts() {
		List<AccountResponseDTO> accounts = accountService.listAll().stream().map(mapper::toAccountResponseDto)
				.collect(toList());
		return new ResponseEntity<List<AccountResponseDTO>>(accounts, HttpStatus.OK);
	}

	@GetMapping(path = "/{accountId}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AccountResponseDTO> getAccount(@PathVariable Integer accountId) {

		AccountResponseDTO account = mapper.toAccountResponseDto(accountService.getById(accountId)) ;

		return new ResponseEntity<AccountResponseDTO>(account, HttpStatus.OK);

	}

	@PostMapping(consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE }, produces = {
			MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AccountResponseDTO> createAccount(@RequestBody AccountRequestDTO req) {
		return new ResponseEntity<AccountResponseDTO>(mapper.toAccountResponseDto(accountService.add(mapper.toAccount(req))) , HttpStatus.CREATED);
	}

	@PutMapping(path = "/{accountId}", consumes = { MediaType.APPLICATION_XML_VALUE,
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_XML_VALUE,
					MediaType.APPLICATION_JSON_VALUE })
	public ResponseEntity<AccountResponseDTO> updateAccount(@PathVariable Integer accountId,
			@RequestBody AccountRequestDTO req) {
		Account account = mapper.toAccount(req);
		accountService.edit(accountId, account);
		return new ResponseEntity<AccountResponseDTO>(mapper.toAccountResponseDto(account) , HttpStatus.OK);
	}

	@DeleteMapping(path = "/{accountId}")
	public ResponseEntity<Void> deleteUser(@PathVariable Integer accountId) {
		accountService.remove(accountId);

		return ResponseEntity.noContent().build();
	}

}
