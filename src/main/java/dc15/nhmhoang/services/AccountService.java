package dc15.nhmhoang.services;

import java.util.List;

import dc15.nhmhoang.models.Account;

public interface AccountService {

	List<Account> listAll();

	Account getById(Integer id);

	Account add(Account account);

	Account edit(Integer id, Account account);

	void remove(Integer id);
}
