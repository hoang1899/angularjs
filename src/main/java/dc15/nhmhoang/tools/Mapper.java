package dc15.nhmhoang.tools;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import dc15.nhmhoang.models.Account;
import dc15.nhmhoang.models.requests.AccountRequestDTO;
import dc15.nhmhoang.models.responses.AccountResponseDTO;

@Component
public class Mapper {
	public AccountResponseDTO toAccountResponseDto(Account account) {
		AccountResponseDTO dto = new AccountResponseDTO();
		dto.setEmail(account.getEmail());
		dto.setGender(StringUtils.capitalize(account.getGender().toString().toLowerCase()));
		dto.setRole(StringUtils.capitalize(account.getRole().toString().toLowerCase()));
		dto.setId(account.getId());
		dto.setName(account.getName());
		dto.setPhone(account.getPhone());
		return dto;
	}

	public Account toAccount(AccountRequestDTO req) {
		Account account = new Account();
		account.setEmail(req.getEmail());
		account.setGender(req.getGender());
		account.setRole(req.getRole());
		account.setName(req.getName());
		account.setPhone(req.getPhone());
		return account;
	}
	
}
